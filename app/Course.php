<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "course";

    protected $fillable = ['c_name', 's_id'];
}
