<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class StudentController extends Controller
{
    public function index()
    {
        $students = Students::latest()->paginate(5);
        return view('studentviews.index',compact('students'))->with('i',(request()->input('page',1)-1)*5);
    }

    public function create($id = null)
    {
        $student = null;
        if($id) {
            $student = Students::find($id);

            return view('studentviews.create', compact(['student']));
        }

        return view('studentviews.create', compact(['student']));
    }

    public function store(Request $request)
    {
        $request->validate([
            'rollnum' => 'required|unique:students,RollNum|numeric|gt:0',
            'name' => 'required|string',
            'batch' => 'required|string',
            'program' => 'required|string',
        ]);


        Students::create([
            'RollNum' => $request->rollnum,
            'Name' => $request->name,
            'Batch' => $request->batch,
            'Program' => strtoupper($request->program)
        ]);

        return redirect()->back()
            ->with('success','Student created successfully.');
    }

    public function show($id)
    {
        $student = Students::find($id);
        return view('studentviews.show',compact('student'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'rollnum' => 'required|numeric|gt:0',
            'name' => 'required|string',
            'batch' => 'required',
            'program' => 'required',
        ]);

        $getdata = Students::find($id);

        $getdata->RollNum = $request->rollnum;
        $getdata->Name = $request->name;
        $getdata->Batch = $request->batch;
        $getdata->Program = strtoupper($request->program);

        $getdata->save();

        return redirect()->action('StudentController@index');
    }

    public function destroy($id)
    {
        $student= Students::find($id);

        $student->delete();

        return redirect()->back()->with('success','Student deleted successfully');
    }

    public function getCourse()
    {
        $student = Students::find(3)->course;

        dd($student);
    }
}
