@extends('studentviews.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Students</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="/create"> Create new Students</a>
            </div>
            <br>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>RollNum</th>
            <th>Name</th>
            <th>Batch</th>
            <th>Program</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($students as $student)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $student->RollNum }}</td>
            <td>{{ $student->Name }}</td>
            <td>{{ $student->Batch }}</td>
            <td>{{ $student->Program }}</td>
            <td>
                
                <form action="/students/{{ $student->id }}" method="POST">
                <a class="btn btn-info" href="/students/{{ $student->id }}" >Show</a>
                <a class="btn btn-primary" href="/create/{{ $student->id }}">Edit</a>
                    @csrf
                    @method('DELETE')  
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $students->links() !!} 
      
@endsection