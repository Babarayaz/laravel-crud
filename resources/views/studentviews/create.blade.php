@extends('studentviews.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>,
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create New Student</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="/"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check your input code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{$student ? '/students/' . $student->id : '/students'}}" method="POST">
        @csrf
        @if($student)
            {{ method_field('PUT') }}
        @endif
        <div class="col">
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>RollNum:</strong>
                    <input type="text" value="{{$student ? $student->RollNum : ''}}" name="rollnum" class="form-control" placeholder="RollNum">
                </div>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" value="{{$student ? $student->Name : ''}}" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Batch:</strong>
                    <input type="text" value="{{$student ? $student->Batch : ''}}" name="batch" class="form-control" placeholder="Batch">
                </div>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>Program:</strong>
                    <input type="text" value="{{$student ? $student->Program : ''}}" name="program" class="form-control" placeholder="Program">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </div>

    </form>
@endsection
