@extends('studentviews.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Students</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="/"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
    @if($student)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>RollNum:</strong>
                {{ $student->RollNum }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $student->Name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Batch:</strong>
                {{ $student->Batch }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Program:</strong>
                {{ $student->Program }}
            </div>
        </div>
    </div>
    @endif
@endsection
