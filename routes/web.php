<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// Route::group(['prefix' => 'students'], function() {
//     Route::get('/{id}', 'StudentController@show');
//     Route::get('/create', "StudentController@create");
//     Route::get('/', 'StudentController@index');
//     Route::delete('/{id}', 'StudentController@destroy');
//     Route::get('/imran', function() {
//         return "hellp";
//     });
// });

Route::get('students/{id}', 'StudentController@show');
Route::get('/create/{id?}', "StudentController@create");
Route::get('/', 'StudentController@index');
Route::delete('students/{id}', 'StudentController@destroy');
Route::post('students', "StudentController@store");
Route::put('students/{id}', "StudentController@update");

Route::get('courses', "StudentController@getCourse");

// Route::resource('students', 'StudentController');
